
<%@page import="br.com.senac.agenda.model.Usuario"%>
<jsp:include page="../header.jsp"/>


<style>

    body { 

        /*
        /* The image used */
        background-image: url("../resources/imagens/lionel-messi-barcelona-champions-league-06062015_djxxtpw7701r1u31x6r1q3df1.jpg");


        /* Full height */
        height: 100%; 

        /* Center and scale the image nicely */
        background-position: center top ;
        background-repeat: no-repeat  ;
        background-attachment: fixed;
        background-size: cover ;
    }

</style>

<%
    Usuario usuario = (Usuario) request.getAttribute("usuario");
    String mensagem = (String) request.getAttribute("mensagem");
    String erro = (String) request.getAttribute("erro");

%>

<% if (mensagem != null) {%>
<div class="alert alert-success" role="alert">
    <%= mensagem%>
</div>
<%}%>

<% if (erro != null) {%>
<div class="alert alert-danger" role="alert">
    <%= erro%>
</div>
<%}%>





<form action="./SalvarUsuarioServlet" method="post">
    <input type="hidden" name="id" value="<%= usuario != null ? usuario.getId() : ""%>" />
    <div class="form-group">
        <label for="codigo"  style=" text-shadow: 1px 1px 2px #333; color: #ffffff" >C�digo</label>
        <input type="text" class="form-control col-2" id="codigo" name="codigo" readonly value="<%= usuario != null ? usuario.getId() : ""%>" >
    </div>
    <div class="form-group">
        <label for="nome"  style=" text-shadow: 1px 1px 2px #333; color: #ffffff" >Nome</label>
        <input type="text" class="form-control" id="nome" placeholder="nome" name="nome" value="<%= usuario != null ? usuario.getNome() : ""%>" >
    </div>
    <div class="form-group">
        <label for="senha"  style=" text-shadow: 1px 1px 2px #333; color: #ffffff" >Senha</label>
        <input type="password" class="form-control" id="senha" placeholder="senha" name="senha">
    </div>
    <div class="form-group text-right">
        <button type="submit" class="btn btn-primary">Salvar</button>
        <button type="reset" class="btn btn-danger">Cancelar</button>
    </div>
</form>


<jsp:include page="../footer.jsp"/>
