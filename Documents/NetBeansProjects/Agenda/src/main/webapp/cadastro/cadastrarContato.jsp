
<%@page import="br.com.senac.agenda.model.Cadastro"%>
<jsp:include page="../header.jsp"/>


<style>

    body { 

        /*
        /* The image used */
        background-image: url("../resources/imagens/lionel-messi-barcelona-champions-league-06062015_djxxtpw7701r1u31x6r1q3df1.jpg");


        /* Full height */
        height: 100%; 

        /* Center and scale the image nicely */
        background-position: center top ;
        background-repeat: no-repeat  ;
        background-attachment: fixed;
        background-size: cover ;
    }

</style>


<%
    Cadastro cadastro = (Cadastro) request.getAttribute("cadastro");
    String mensagem = (String) request.getAttribute("mensagem");
    String erro = (String) request.getAttribute("erro");

%>

<% if (mensagem != null) {%>
<div class="alert alert-success" role="alert">
    <%= mensagem%>
</div>
<%}%>

<% if (erro != null) {%>
<div class="alert alert-danger" role="alert">
    <%= erro%>
</div>
<%}%>


<fieldset style=" text-shadow: 1px 1px 2px #333; color: #ffffff">
    <form action="./CadastrarContatoServlet" method="post">
        <input type="hidden" name="id" value="<%= cadastro != null ? cadastro.getId() : ""%>" />
        <div class="row">
            <div class="col-2">
                <label for="codigo">C�digo</label>
                <input type="text" class="form-control" id="codigo" name="codigo" readonly value="<%= cadastro != null ? cadastro.getId() : ""%>" >
            </div>
        </div>



        <div >
            <div class="form-group" >
                <label for="nome">Nome</label>
                <input type="text" class="form-control" id="nome" placeholder="nome" name="nome" value="<%= cadastro != null ? cadastro.getNome() : ""%>" >
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <div class="form-group">
                    <label for="telefone">Telefone</label>
                    <input type="text" class="form-control" id="telefone" placeholder="telefone" name="telefone" value="<%= cadastro != null ? cadastro.getTelefone() : ""%>" >
                </div>
            </div>
            <div class="col-4">
                <label for="celular">Celular</label>
                <input type="text" class="form-control" id="celular" placeholder="celular" name="celular" value="<%= cadastro != null ? cadastro.getCelular() : ""%>" >
            </div>

            <div class="col-4">
                <label for="fax">Fax</label>
                <input type="text" class="form-control" id="fax" placeholder="fax" name="fax" value="<%= cadastro != null ? cadastro.getFax() : ""%>" >
            </div>
        </div> 

        <div class="row">
            <div class="col-3">
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" id="cep" placeholder="cep" name="cep" value="<%= cadastro != null ? cadastro.getCep() : ""%>" >
                </div>
            </div>
            <div class="col-6">
                <label for="endereco">Endere�o</label>
                <input type="text" class="form-control" id="endereco" placeholder="endereco" name="endereco" value="<%= cadastro != null ? cadastro.getEndereco() : ""%>"  >
            </div>
            <div class="col-3">
                <label for="numero">N�mero</label>
                <input type="text" class="form-control" id="numero" placeholder="numero" name="numero" value="<%= cadastro != null ? cadastro.getNumero() : ""%>"  >
            </div>
        </div>

        <div class="row">
            <div class="col-5">
                <div class="form-group">
                    <label for="bairro">Bairro</label>
                    <input type="text" class="form-control" id="bairro" placeholder="bairro" name="bairro" value="<%= cadastro != null ? cadastro.getBairo() : ""%>" >
                </div>
            </div>
            <div class="col-5">
                <label for="cidade">Cidade</label>
                <input type="text" class="form-control" id="cidade" placeholder="cidade" name="cidade" value="<%= cadastro != null ? cadastro.getCidade() : ""%>"  >
            </div>
            <div class="col-2">
                <label for="uf">UF</label>
                <input type="text" class="form-control" id="uf" placeholder="uf" name="uf" value="<%= cadastro != null ? cadastro.getUf() : ""%>" >
            </div>
        </div>
        <div> 
            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="text" class="form-control" id="email" placeholder="email" name="email" value="<%= cadastro != null ? cadastro.getEmail() : ""%>" >
            </div>
        </div>


        <div class="form-group text-right">
            <button type="submit" class="btn btn-primary">Salvar</button>
            <button type="reset" class="btn btn-danger">Cancelar</button>
        </div>
    </form>
</fieldset>

<jsp:include page="../footer.jsp"/>

