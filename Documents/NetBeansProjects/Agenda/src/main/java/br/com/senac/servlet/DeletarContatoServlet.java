/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.agenda.dao.CadastroDAO;
import br.com.senac.agenda.model.Cadastro;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sala302b
 */
@WebServlet(name = "DeletarContatoServlet", urlPatterns = {"/cadastro/DeletarContatoServlet"})
public class DeletarContatoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id = request.getParameter("id");
        String formNome = request.getParameter("formNome");
        String formId = request.getParameter("formId");
        String formUf = request.getParameter("formUf");
        String erro = null;

        try {

            Integer codigo = null;
            if (id != null && !id.trim().isEmpty()) {

                codigo = Integer.parseInt(id);
            }

            Integer idPesquisa = null;
            if (formId != null && !formId.trim().isEmpty()) {

                idPesquisa = Integer.parseInt(formId);
            }

            String nome = null;

            if (formNome != null && !formNome.trim().isEmpty()) {
                nome = formNome;
            }

            String uf = null;

            if (formUf != null && !formUf.trim().isEmpty()) {
                uf = formUf;
            }
            
            CadastroDAO dao = new CadastroDAO();

            Cadastro cadastro = new Cadastro();
            cadastro.setId(codigo);

            dao.deletar(cadastro);

            String mensagem = "Removido com sucesso.";

            List<Cadastro> lista = dao.getByFiltro(idPesquisa, nome, uf);

            request.setAttribute("lista", lista);
            request.setAttribute("mensagem", mensagem);

        } catch (Exception ex) {
            erro = "Contato não encontrado.";
            request.setAttribute("erro", erro);
            ex.printStackTrace();
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("pesquisarContato.jsp");

        dispatcher.forward(request, response);

    }
}


