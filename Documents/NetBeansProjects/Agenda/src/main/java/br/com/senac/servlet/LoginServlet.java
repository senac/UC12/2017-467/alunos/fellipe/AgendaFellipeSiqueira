package br.com.senac.servlet;

import br.com.senac.agenda.dao.UsuarioDAO;
import br.com.senac.agenda.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest requisicao, HttpServletResponse resposta) throws ServletException, IOException {

        String usuario = requisicao.getParameter("user");
        String navegador = requisicao.getParameter("User-Agent");

        //tipo de resposta
        resposta.setContentType("text/html");
        PrintWriter out = resposta.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("Olá , " + usuario + "</b><br/>");
        out.println("Seja Bem-Vindo ao Mundo do Servlets!!!!!");
        out.println("Você esta usando o navegador: " + navegador);
        out.println("</body>");
        out.println("</html>");

    }

    @Override
    protected void doPost(HttpServletRequest requisicao, HttpServletResponse resposta) throws ServletException, IOException {
        String nome = requisicao.getParameter("usuario");
        String senha = requisicao.getParameter("senha");

        PrintWriter out = resposta.getWriter();

        out.println("<body>");
        out.println("<html>");

        if ((nome != null && senha != null)/* se nao faor nulo*/ && (!nome.trim().isEmpty() && !senha.trim().isEmpty())) /*s for nulo */ {

            UsuarioDAO dao = new UsuarioDAO();
            Usuario usuario = dao.getByName(nome);

            if (usuario != null && usuario.getSenha().equals(senha)) {
                // Caso não exista sessao o container vai criar
                // caso exista ele vai somente devolver o objeto
                HttpSession session = requisicao.getSession();
                session.setAttribute("user", usuario);
                session.getAttribute("user");

                //  out.println("Seja bem-vindo " + usuario.getNome());
                resposta.sendRedirect("principal.jsp");
            } else {
                if (usuario == null) {
                    out.println("Usuário Inexistente!!!");
                } else {
                    out.println("Falha na autenticação");
                }

            }
            out.println("</body>");
            out.println("</html>");

        } else {
            //retornar para login

            resposta.sendRedirect("login.html");
        }

        /*  Usuario usuario = new Usuario();

            PrintWriter out = resposta.getWriter();
            out.println("<html>");
            out.println("<body>");
            if (usuario.getNome().equals("Messi") && usuario.getSenha().equals("123")) {
                out.println("Seja bem-vindo " + usuario.getNome());
            } else {
                out.println("Falha na autenticação");
            }
            out.println("</body>");
            out.println("</html");
         */
    }

}
